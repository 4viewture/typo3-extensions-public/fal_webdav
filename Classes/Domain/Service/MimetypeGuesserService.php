<?php

namespace TYPO3\FalWebdav\Domain\Service;


use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class MimetypeGuesserService
{
    protected $mimetypes = null;

    public function guess($fileExtension)
    {
        if ($this->mimetypes === null) {
            $file = ExtensionManagementUtility::extPath('fal_webdav') . '/Resources/Private/MimeTypes/mime.types.txt';
            $this->initMimeTypesFromFile($file);
        }
        if (isset($this->mimetypes[strtolower($fileExtension)])) {
            return $this->mimetypes[strtolower($fileExtension)];
        }
        return '';
    }

    protected function initMimeTypesFromFile($file)
    {
        $this->mimetypes = [];
        $fp = fopen($file, 'rb');
        while(($line = fgets($fp)) !== false) {
            $line = rtrim($line, "\r\n");
            if (substr($line, 0, 1) !== '#') {
                list($mimeType, $extensionsString) = explode(chr(9), $line, 2);

                $extensions = explode(' ', $extensionsString);
                foreach($extensions as $extension) {
                    $this->mimetypes[trim($extension)] = $mimeType;
                }
            }
        }
        fclose($fp);
    }
}